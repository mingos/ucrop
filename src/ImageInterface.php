<?php
/* uCrop
 * Copyright (c) 2014 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace Mingos\uCrop;

/**
 * Every image needs to implement this interface to be usable
 *
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 */
interface ImageInterface
{
	/**
	 * Resize image without maintaining aspect ratio
	 *
	 * @param  integer $w Target width; null = leave at original width
	 * @param  integer $h Target height; null = leave at original height
	 * @return self
	 */
	public function resize($w, $h);

	/**
	 * Scale an image proportionally
	 *
	 * @param  integer $w         Width
	 * @param  integer $h         Height
	 * @param  boolean $fitInside Whether to fit inside the specified dimensions
	 * @return self
	 */
	public function scale($w, $h, $fitInside = false);

	/**
	 * Crop the image using either the current gravity setting or manually specified crop start position
	 *
	 * @param  integer      $w Desired width
	 * @param  integer      $h Desired height
	 * @param  null|integer $x Crop starting X position
	 * @param  null|integer $y Crop strting Y position
	 * @return self
	 */
	public function crop($w, $h, $x = null, $y = null);

	/**
	 * Crop the image with the crop start position specified as relative to the current gravity setting
	 *
	 * @param  integer $w Desired width
	 * @param  integer $h Desired height
	 * @param  integer $x Crop starting X position relative to the current gravity setting
	 * @param  integer $y Crop starting Y position relative to the current gravity setting
	 * @return self
	 */
	public function cropRelative($w, $h, $x, $y);

	/**
	 * Flip the image using one of the avilable modes
	 *
	 * @param  integer $mode Mode indicating a horizontal and/or vertical flip
	 * @return self
	 */
	public function flip($mode);

	/**
	 * Flip te image horizontally
	 *
	 * @return self
	 */
	public function flipHorizontal();

	/**
	 * Flip the image vertically
	 *
	 * @return self
	 */
	public function flipVertical();

	/**
	 * Flip the image both horizontally and vertically
	 *
	 * @return self
	 */
	public function flipBoth();

	/**
	 * Output the image as file
	 *
	 * @param  string $filename Target file name
	 * @return self
	 */
	public function save($filename);

	/**
	 * Set gravity for cropping images
	 *
	 * @param  integer $gravity Integer gravity,
	 * @return self
	 */
	public function setGravity($gravity);

	/**
	 * Overlay images, placing the one passed in as parametre on top
	 *
	 * @param  ImageInterface $image An image that should be overlaid
	 * @return self
	 */
	public function overlay(ImageInterface $image);

	/**
	 * Fetch the current image width
	 *
	 * @return integer
	 */
	public function getWidth();

	/**
	 * Fetch the current image height
	 *
	 * @return integer
	 */
	public function getHeight();

	/**
	 * Fetch the raw image resource (or Imagick object)
	 *
	 * @return mixed
	 */
	public function getImage();
}
