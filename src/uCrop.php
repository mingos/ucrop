<?php
/* uCrop
 * Copyright (c) 2014 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace Mingos\uCrop;

use Mingos\uCrop\Exception\MissingLibraryException;

/**
 * Main uCrop class, acting as an image factory.
 *
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 */
class uCrop
{
	/**
	 * Library version
	 * @var string
	 */
	const VERSION = "1.0.0";

	/**#@+
	 * Library types
	 */
	const LIBRARY_IMAGICK = "Imagick";
	const LIBRARY_GD = "Gd";
	/**#@-*/

	/**#@+
	 * Setting names
	 */
	const SETTING_COMPRESSION_QUALITY = "compressionQuality";
	const SETTING_GRAVITY = "gravity";
	const SETTING_LIBRARY = "library";
	/**#@-*/

	/**#@+
	 * Flip modes
	 */
	const FLIP_HORIZONTAL = 1;
	const FLIP_VERTICAL = 2;
	const FLIP_BOTH = 3;
	/**#@-*/

	/**#@+
	 * Gravity values
	 */
	const GRAVITY_NORTHWEST = 1;
	const GRAVITY_NORTH = 2;
	const GRAVITY_NORTHEAST = 3;
	const GRAVITY_WEST = 4;
	const GRAVITY_CENTRE = 5;
	const GRAVITY_EAST = 6;
	const GRAVITY_SOUTHWEST = 7;
	const GRAVITY_SOUTH = 8;
	const GRAVITY_SOUTHEAST = 9;
	/**#@-*/

	private $settings = array();

	/**
	 * Create an instance of uCrop and build default settings
	 *
	 * @param  array                   $settings The settings array
	 * @throws MissingLibraryException           No image processing library is found
	 */
	public function __construct(array $settings = array())
	{
		if (extension_loaded("imagick")) {
			$library = self::LIBRARY_IMAGICK;
		} else if (extension_loaded("gd")) {
			$library = self::LIBRARY_GD;
		} else {
			throw new MissingLibraryException("Neither ImageMagick nor GD libraries found.", 500);
		}

		// default settings
		$this->settings = $settings + array(
			uCrop::SETTING_LIBRARY => $library,
			uCrop::SETTING_COMPRESSION_QUALITY => 75,
			uCrop::SETTING_GRAVITY => uCrop::GRAVITY_CENTRE
		);
	}

	/**
	 * Instantiate a new image
	 *
	 * @param  string         $filename Input image filename
	 * @param  array          $settings Settings array
	 * @return ImageInterface
	 */
	public function image($filename, array $settings = array()) {
		$class = "\\Mingos\\uCrop\\Image\\{$this->settings[uCrop::SETTING_LIBRARY]}";

		return new $class($filename, $settings + $this->settings);
	}
}
