<?php
/* uCrop
 * Copyright (c) 2014 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace Mingos\uCrop\Image;

use Mingos\uCrop\AbstractImage;
use Mingos\uCrop\ImageInterface;
use Mingos\uCrop\uCrop;

/**
 * Image processor adapter using ImageMagick
 *
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 */
class Imagick extends AbstractImage {
	/**
	 * @inheritdoc
	 */
	public function setImage($filename)
	{
		$this->image = new \Imagick($filename);
		$this->image->stripImage();

		$this->width = $this->image->getImageWidth();
		$this->height = $this->image->getImageHeight();
	}

	/**
	 * @inheritdoc
	 */
	public function resize($w, $h)
	{
		$w = $w > 0 ? $w : $this->width;
		$h = $h > 0 ? $h : $this->height;

		$this->image->scaleImage($w, $h);
		$this->width = $w;
		$this->height = $h;

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function scale($w, $h, $fitInside = false)
	{
		if ($w > 0 && $h > 0) {
			$proportionW = $this->width / $w;
			$proportionH = $this->height / $h;

			if ($proportionW > $proportionH) {
				$this->image->scaleImage($fitInside ? $w : 0, $fitInside ? 0 : $h);
			} else {
				$this->image->scaleImage($fitInside ? 0 : $w, $fitInside ? $h : 0);
			}
		} else {
			$this->image->scaleImage($w, $h);
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function crop($w, $h, $x = null, $y = null)
	{
		list($x, $y) = $this->calculateCropStartingPoint($w, $h, $x, $y);
		$this->image->cropImage($w, $h, $x, $y);
		$this->width = $w;
		$this->height = $h;
		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function cropRelative($w, $h, $x, $y)
	{
		list($outX, $outY) = $this->calculateCropStartingPoint($w, $h);

		$x += $outX;
		$y += $outY;

		$x = max(0, min($this->width - $w, $x));
		$y = max(0, min($this->height - $h, $y));

		$this->image->cropImage($w, $h, $x, $y);
		$this->width = $w;
		$this->height = $h;

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function flip($mode)
	{
		switch ($mode) {
			case uCrop::FLIP_HORIZONTAL:
				$this->image->flopImage();
				break;
			case uCrop::FLIP_VERTICAL:
				$this->image->flipImage();
				break;
			case uCrop::FLIP_BOTH:
				$this->image->flopImage();
				$this->image->flipImage();
				break;
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function overlay(ImageInterface $image)
	{
		$this->image->compositeImage(
			$image->scale($this->width, $this->height)->getImage(),
			\Imagick::COMPOSITE_DEFAULT, 0, 0
		);

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function save($filename)
	{
		$this->image->setCompressionQuality($this->settings[uCrop::SETTING_COMPRESSION_QUALITY]);
		$this->image->writeImage($filename);
	}
}
