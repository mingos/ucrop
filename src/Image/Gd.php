<?php
/* uCrop
 * Copyright (c) 2014 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace Mingos\uCrop\Image;

use Mingos\uCrop\AbstractImage;
use Mingos\uCrop\Exception\ImageFormatException;
use Mingos\uCrop\ImageInterface;
use Mingos\uCrop\uCrop;

/**
 * Image processor adapter using GD2
 *
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 */
class Gd extends AbstractImage {
	/**
	 * @inheritdoc
	 */
	public function setImage($filename)
	{
		$imagesize = getimagesize($filename);
		$mimeType = isset($imagesize["mime"]) ? $imagesize["mime"] : null;

		switch ($mimeType) {
			case "image/gif":
				$this->image = imagecreatefromgif($filename);
				break;
			case "image/jpeg":
				$this->image = imagecreatefromjpeg($filename);
				break;
			case "image/png":
				$this->image = imagecreatefrompng($filename);
				break;
			default:
				throw new ImageFormatException("Unsupported image format.");
		}

		$this->width = $imagesize[0];
		$this->height = $imagesize[1];
	}

	/**
	 * @inheritdoc
	 */
	public function resize($w, $h)
	{
		$w = $w > 0 ? $w : $this->width;
		$h = $h > 0 ? $h : $this->height;

		$newImage = imagecreatetruecolor($w, $h);

		imagealphablending($newImage, false);
		$transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 0);
		imagefill($newImage, 0, 0, $transparent);
		imagesavealpha($newImage, true);

		imagecopyresampled($newImage, $this->image, 0, 0, 0, 0, $w, $h, $this->width,$this->height);

		imagedestroy($this->image);
		$this->image = $newImage;
		$this->width = $w;
		$this->height = $h;

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function scale($w, $h, $fitInside = false)
	{
		// get expected dimensions with one = 0
		if ($w > 0 && $h > 0) {
			$proportionW = $this->width / $w;
			$proportionH = $this->height / $h;

			if ($proportionW > $proportionH) {
				$w = $fitInside ? $w : 0;
				$h = $fitInside ? 0 : $h;
			} else {
				$w = $fitInside ? 0 : $w;
				$h = $fitInside ? $h : 0;
			}
		}

		// calculate the missing dimension
		if ($w == 0) {
			$w = floor($this->width * $h / $this->height);
		} else {
			$h = floor($this->height * $w / $this->width);
		}

		return $this->resize($w, $h);
	}

	/**
	 * @inheritdoc
	 */
	public function crop($w, $h, $x = null, $y = null)
	{
		list($x, $y) = $this->calculateCropStartingPoint($w, $h, $x, $y);

		$newImage = imagecreatetruecolor($w, $h);

		imagealphablending($newImage, false);
		$transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 0);
		imagefill($newImage, 0, 0, $transparent);
		imagesavealpha($newImage, true);

		imagecopy($newImage, $this->image, 0, 0, $x, $y, $w, $h);

		imagedestroy($this->image);
		$this->image = $newImage;
		$this->width = $w;
		$this->height = $h;

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function cropRelative($w, $h, $x, $y)
	{
		list($outX, $outY) = $this->calculateCropStartingPoint($w, $h, null, null);

		$x += $outX;
		$y += $outY;

		$x = max(0, min($this->width - $w, $x));
		$y = max(0, min($this->height - $h, $y));

		$newImage = imagecreatetruecolor($w, $h);

		imagealphablending($newImage, false);
		$transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 0);
		imagefill($newImage, 0, 0, $transparent);
		imagesavealpha($newImage, true);

		imagecopy($newImage, $this->image, 0, 0, $x, $y, $w, $h);

		imagedestroy($this->image);
		$this->image = $newImage;
		$this->width = $w;
		$this->height = $h;

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function flipHorizontal()
	{
		if (function_exists("imageflip")) {
			imageflip($this->image, IMG_FLIP_HORIZONTAL);
		} else {
			$temp = imagecreatetruecolor($this->width, $this->height);
			imagecopyresampled(
				$temp,
				$this->image,
				0,
				0,
				$this->width - 1,
				0,
				$this->width,
				$this->height,
				0 - $this->width,
				$this->height
			);

			imagedestroy($this->image);
			$this->image = $temp;
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function flipVertical()
	{
		if (function_exists("imageflip")) {
			imageflip($this->image, IMG_FLIP_VERTICAL);
		} else {
			$temp = imagecreatetruecolor($this->width, $this->height);
			imagecopyresampled(
				$temp,
				$this->image,
				0,
				0,
				0,
				$this->height - 1,
				$this->width,
				$this->height,
				$this->width,
				0 - $this->height
			);

			imagedestroy($this->image);
			$this->image = $temp;
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function flipBoth()
	{
		if (function_exists("imageflip")) {
			imageflip($this->image, IMG_FLIP_BOTH);
		} else {
			$temp = imagecreatetruecolor($this->width, $this->height);
			imagecopyresampled(
				$temp,
				$this->image,
				0,
				0,
				$this->width - 1,
				$this->height - 1,
				$this->width,
				$this->height,
				0 - $this->width,
				0 - $this->height
			);

			imagedestroy($this->image);
			$this->image = $temp;
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function flip($mode)
	{
		switch ($mode) {
			case uCrop::FLIP_HORIZONTAL:
				$this->flipHorizontal();
				break;
			case uCrop::FLIP_VERTICAL:
				$this->flipVertical();
				break;
			case uCrop::FLIP_BOTH:
				$this->flipBoth();
				break;
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function overlay(ImageInterface $image)
	{
		// original background image
		$background = $this->image;
		imagealphablending($this->image, true);
		$tw = $this->width;
		$th = $this->height;

		/**
		 * @var resource $overlay
		 */
		$overlay = $image->getImage();
		$sw = $image->getWidth();
		$sh = $image->getHeight();

		// canvas to put images together on
		$canvas = imagecreatetruecolor($tw, $th);
		imagealphablending($canvas, false);
		imagesavealpha($canvas, true);
		$transparent = imagecolorallocatealpha($canvas, 255, 255, 255, 127);
		imagefill($canvas, 0, 0, $transparent);
		imagecopy($canvas, $overlay, 0, 0, 0, 0, $sw, $sh);

		//overlay the images
		imagecopy($background, $canvas, 0, 0, 0, 0, $tw, $th);

		//clean up
		imagedestroy($canvas);
		imagedestroy($overlay);

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function save($filename)
	{
		$format = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

		switch ($format) {
			case "jpg":
			case "jpeg":
				imagejpeg($this->image, $filename, $this->settings[uCrop::SETTING_COMPRESSION_QUALITY]);
				break;
			case "png":
				imagepng($this->image, $filename);
				break;
			case "gif":
				imagegif($this->image, $filename);
				break;
			default:
				throw new ImageFormatException("Unsupported output format.");
		}
	}
}
