<?php
/* uCrop
 * Copyright (c) 2014 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace Mingos\uCrop;

/**
 * Base class for all builtin image adapters
 *
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 */
abstract class AbstractImage implements ImageInterface
{
	/**
	 * The image being processed
	 * @var mixed
	 */
	protected $image;

	/**
	 * Image width
	 * @var integer
	 */
	protected $width;

	/**
	 * Image height
	 * @var integer
	 */
	protected $height;

	/**
	 * Settings array
	 * @var array
	 */
	protected $settings;

	/**
	 * Instantiate an image
	 *
	 * @param string $filename Original image file name
	 * @param array  $settings Custom settings
	 */
	public function __construct($filename, array $settings = array())
	{
		$this->setImage($filename);
		$this->setSettings($settings);
	}

	/**
	 * Set the original image.
	 *
	 * @param string $filename File name of the original image
	 */
	abstract protected function setImage($filename);

	/**
	 * Set default settings and extend them with the passed in settings
	 *
	 * @param array $settings the settings array
	 */
	protected function setSettings(array $settings)
	{
		$this->settings = array(
			uCrop::SETTING_GRAVITY => uCrop::GRAVITY_CENTRE,
			uCrop::SETTING_COMPRESSION_QUALITY => 75
		);

		foreach ($settings as $setting => $value) {
			$this->settings[$setting] = $value;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function getImage()
	{
		return $this->image;
	}

	/**
	 * @inheritdoc
	 */
	public function getWidth()
	{
		return $this->width;
	}

	/**
	 * @inheritdoc
	 */
	public function getHeight()
	{
		return $this->height;
	}

	/**
	 * @inheritdoc
	 */
	public function setGravity($gravity)
	{
		$this->settings[uCrop::SETTING_GRAVITY] = intval($gravity);

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function flipHorizontal()
	{
		return $this->flip(uCrop::FLIP_HORIZONTAL);
	}

	/**
	 * @inheritdoc
	 */
	public function flipVertical()
	{
		return $this->flip(uCrop::FLIP_VERTICAL);
	}

	/**
	 * @inheritdoc
	 */
	public function flipBoth()
	{
		return $this->flip(uCrop::FLIP_BOTH);
	}

	/**
	 * Calculate where the cropping should start at
	 *
	 * @param  integer $w Cropped image width
	 * @param  integer $h Cropped image height
	 * @param  integer $x Cropping start X position
	 * @param  integer $y Cropping start Y position
	 * @return array
	 */
	protected function calculateCropStartingPoint($w, $h, $x = null, $y = null)
	{
		switch ($this->settings[uCrop::SETTING_GRAVITY]) {
			case uCrop::GRAVITY_NORTHWEST:
				$x = is_null($x) ? 0 : $x;
				$y = is_null($y) ? 0 : $y;
				break;
			case uCrop::GRAVITY_NORTH:
				$x = is_null($x) ? floor(($this->width - $w) / 2) : $x;
				$y = is_null($y) ? 0 : $y;
				break;
			case uCrop::GRAVITY_NORTHEAST:
				$x = is_null($x) ? $this->width - $w : $x;
				$y = is_null($y) ? 0 : $y;
				break;
			case uCrop::GRAVITY_WEST:
				$x = is_null($x) ? 0 : $x;
				$y = is_null($y) ? floor(($this->height - $h) / 2) : $y;
				break;
			case uCrop::GRAVITY_CENTRE:
				$x = is_null($x) ? floor(($this->width - $w) / 2) : $x;
				$y = is_null($y) ? floor(($this->height - $h) / 2) : $y;
				break;
			case uCrop::GRAVITY_EAST:
				$x = is_null($x) ? $this->width - $w : $x;
				$y = is_null($y) ? floor(($this->height - $h) / 2) : $y;
				break;
			case uCrop::GRAVITY_SOUTHWEST:
				$x = is_null($x) ? 0 : $x;
				$y = is_null($y) ? $this->height - $h : $y;
				break;
			case uCrop::GRAVITY_SOUTH:
				$x = is_null($x) ? floor(($this->width - $w) / 2) : $x;
				$y = is_null($y) ? $this->height - $h : $y;
				break;
			case uCrop::GRAVITY_SOUTHEAST:
				$x = is_null($x) ? $this->width - $w : $x;
				$y = is_null($y) ? $this->height - $h : $y;
				break;
		};

		return array($x, $y);
	}
}
