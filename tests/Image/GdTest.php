<?php
/* uCrop
 * Copyright (c) 2014 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace Mingos\uCrop\Image;

class GdTest extends \PHPUnit_Framework_TestCase
{
	public function filenameProvider()
	{
		return array(
			array(__DIR__ . "/../images/test.png"),
			array(__DIR__ . "/../images/test.gif"),
			array(__DIR__ . "/../images/test.jpg")
		);
	}

	/**
	 * @param string $filename
	 *
	 * @dataProvider filenameProvider
	 */
	public function testImageCreation($filename)
	{
		$image = new Gd($filename);
		$this->assertInstanceOf('Mingos\uCrop\Image\Gd', $image);
		$this->assertEquals(640, $image->getWidth());
		$this->assertEquals(480, $image->getHeight());
	}

	public function testWrongImageCreation()
	{
		$this->setExpectedException('Mingos\uCrop\Exception\ImageFormatException');
		new Gd(__DIR__ . "/../images/test.txt");
	}

	public function testFlip()
	{
		$image = new Gd(__DIR__ . "/../images/flip.png");

		$image->flipHorizontal();
		$this->assertEquals(0xFF0000, imagecolorat($image->getImage(), 0, 0));
		$this->assertEquals(0x000000, imagecolorat($image->getImage(), 1, 0));

		$image->flipVertical();
		$this->assertEquals(0x0000FF, imagecolorat($image->getImage(), 0, 0));
		$this->assertEquals(0x00FF00, imagecolorat($image->getImage(), 1, 0));

		$image->flipBoth();
		$this->assertEquals(0x000000, imagecolorat($image->getImage(), 0, 0));
		$this->assertEquals(0xFF0000, imagecolorat($image->getImage(), 1, 0));
	}
}
